
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var router = express.Router();
var app = express();
const fs = require("fs");
var server = require('http').Server(app)
app.set('port', (process.env.PORT || '4000'));

app.use(cookieParser());
app.use(cookieParser('brad'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

/*app.get('*', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
}); */
/*app.post('/getTimeInFormate', content.getTimeInFormate);*/
//server port handles
/*var server = app.listen(env.port, function() {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)
});*/
server.listen(app.get('port'), function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)
    //console.log('listening on *:9000');
});