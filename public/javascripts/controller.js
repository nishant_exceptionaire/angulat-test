app.controller('Ctrl', ['$scope', '$http', '$routeParams', '$location', '$filter','$timeout',
    function ($scope, $http, $routeParams, $location, $filter,$timeout) {
        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.data = [];
        $scope.posts = [];
        $scope.q = '';
        $scope.init = function () {
            $scope.postList();
        };

         $scope.postList = function () {
            $http.get("https://jsonplaceholder.typicode.com/posts").then(function (response) {
                if (response.status == 200) {
                    $scope.posts = response.data;
                } else {
                    console.log("error occure");
                }
            })
         };

        $scope.passerrorMsgAlert = false;
        $scope.passsuccessMsgAlert = false;
        $scope.savePost = function(title,body){
            if(title == undefined || body == undefined || title == '' || body == ''){
                $scope.passerrorMsgAlert = true;
                    $scope.passerrorMsgs = "Please enter mandatory(*) fields.";
                $timeout(function() {
                    $scope.passerrorMsgAlert = false;
                }, 3000);
            }else{
                $http({
                    url: 'https://jsonplaceholder.typicode.com/posts',
                    method: 'POST',
                    data: { "title":title,"body":body, "userId":1 },
                    headers: { 'Content-Type': 'application/json' }
                }).then(function(response) {
                    if (response.status == 201) {
                        $scope.passsuccessMsgAlert = true;
                        $scope.passsuccessMsgs = "Post added successfully. You can see added one in console.";
                        $timeout(function() {
                            $scope.passsuccessMsgAlert = false;
                        }, 5000);
                        $scope.posts.push(response.data); 
                        $scope.title = "";
                        $scope.body = ""; 
                        console.log($scope.posts[100]) 
                                        
                    }
                });
            }
        } 

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        }

        $scope.viewPosts = function (id) {
            $location.path("/post/details/"+ id);
        }

        $scope.post_data = function () {

           $http.get("https://jsonplaceholder.typicode.com/posts/"+$routeParams.id).then(function (response) {
                if (response.status == 200) {
                    $scope.id = response.data.id; 
                   $scope.title = response.data.title;
                   $scope.body = response.data.body;
                } else {
                    console.log("error occured");
                }
            })
        }

        $scope.updatePost = function (title,body,id) {
            if(title == undefined || body == undefined || title == '' || body == ''){
                $scope.passerrorMsgAlert = true;
                    $scope.passerrorMsgs = "Please enter mandatory(*) fields.";
                $timeout(function() {
                    $scope.passerrorMsgAlert = false;
                }, 3000);
            }else{
                $http({
                    url: 'https://jsonplaceholder.typicode.com/posts/'+id,
                    method: 'PUT',
                    data: { "title":title,"body":body, "id":id },
                    headers: { 'Content-Type': 'application/json' }
                }).then(function(response) {
                    if (response.status == 200) {
                        $scope.passsuccessMsgAlert = true;
                        $scope.passsuccessMsgs = "Post updated successfully. You can see updated one in console.";
                        $timeout(function() {
                            $scope.passsuccessMsgAlert = false;
                        }, 5000);
                        console.log(response.data)      
                    }
                });
            }
        }

        $scope.editPosts = function (id) {
            $location.path("/edit/post/"+ id);
        }

        $scope.addPosts = function (id) {
            $location.path("/add/post");
        }

        $scope.getData = function () {
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            return $filter('filter')($scope.posts, $scope.q)

        }
    }]);