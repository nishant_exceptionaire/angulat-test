var app = angular.module('app', ['ngRoute']);
/*
app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/posts');

        $stateProvider
            .state("posts", {
                url: "/posts",
                views: {
                    'body': {
                        templateUrl: "../views/post.html",
                        controller: "Ctrl"
                    }
                }
            })
    }])*/
app.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});


app.config(function ($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl: "../views/post.html",
        controller: 'Ctrl'
    })
    .when("/post/details/:ID", {
        templateUrl: "../views/comments.html",
        controller: 'CtrlTwo'
    }).when("/add/post", {
        templateUrl: "../views/add_post.html",
        controller: 'Ctrl'
    }).when("/edit/post/:id", {
        templateUrl: "../views/edit_post.html",
        controller: 'Ctrl'
    })
    
});
