app.controller('CtrlTwo', ['$scope', '$http', '$routeParams', '$location', '$filter',
    function ($scope, $http, $routeParams, $location, $filter) {
        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.comments = [];
        $scope.q = '';
        
        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        }

        $scope.getData = function () {
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            return $filter('filter')($scope.comments, $scope.q)

        }

        $scope.getPostById = function () {
            $scope.ID = $routeParams.ID;
            $http.get("https://jsonplaceholder.typicode.com/posts/" + $scope.ID).then(function (response) {
                if (response.status == 200) {
                    $scope.post = response.data;
                    $scope.getComments($scope.ID);
                } else {
                    console.log("error occured")
                }
            })
        }

        $scope.getComments = function (id) {
            $http.get("https://jsonplaceholder.typicode.com/posts/" + id + "/comments").then(function (response) {
                if (response.status == 200) {
                    $scope.comments = response.data;
                } else {
                    console.log("error occured")
                }
            })
        }



    }]);